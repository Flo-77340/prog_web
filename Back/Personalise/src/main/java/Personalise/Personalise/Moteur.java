package Personalise.Personalise;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
public class Moteur {


    /*
     * IDentifiant
     */
    private Long id;

    @Id
    @Column(name="moteur_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId()
    {   return id;  }

    public void setId(Long id ){ this.id = id; }

    private String picture;
    @Column(name="picture_Name")
    public String getPicture(){return picture;}
    public void setPicture(String picture){this.picture = picture;}

    public Moteur(String picture){
        this.picture = picture;
    }

    public Moteur(){
        this("Jante.default.format");
    }


}
