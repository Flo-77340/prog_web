package Personalise.Personalise;

import org.springframework.data.repository.CrudRepository;

public interface Modele_repository extends CrudRepository<Modele, Long> {
}
