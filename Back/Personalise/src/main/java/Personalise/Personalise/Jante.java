package Personalise.Personalise;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
public class Jante {


    /*
     * IDentifiant
     */
    private Long id;

    @Id
    @Column(name="jante_id")
    @GeneratedValue(strategy = GenerationType.AUTO) public Long getId()
    {   return id;  }

    private String picture;
    @Column(name="picture_Name")
    public String getPicture(){return picture;}
    public void setPicture(String picture){this.picture = picture;}

    public Jante(String picture){
        super();
        this.picture = picture;
    }

    public Jante(Jante jante){
        super();
        this.picture = jante.picture;
    }

    public Jante(){
        this("Jante.default.format");
    }


    public void setId(Long id ){ this.id = id; }

}
