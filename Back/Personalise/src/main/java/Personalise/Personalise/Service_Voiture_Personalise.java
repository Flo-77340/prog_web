package Personalise.Personalise;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collection;

@RestController
public class Service_Voiture_Personalise {

    private static final Logger log = LoggerFactory.getLogger(PersonaliseApplication.class);

    Pneu_repository pneu_repository;
    Moteur_repository moteur_repository;
    Modele_repository modele_repository;
    Jante_repository jante_repository;
    Couleur_repository couleur_repository;

    @Autowired
    public Service_Voiture_Personalise(Pneu_repository pneu_repository,
                                       Moteur_repository moteur_repository,
                                       Modele_repository modele_repository,
                                       Jante_repository jante_repository,
                                       Couleur_repository couleur_repository) {
        super();
        this.pneu_repository = pneu_repository;
        this.moteur_repository=moteur_repository;
        this.modele_repository = modele_repository;
        this.jante_repository = jante_repository;
        this.couleur_repository = couleur_repository;
    }


    @GetMapping("/personalise/pneus")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public Collection<Pneu> getPneusPersonalise(){

        Collection<Pneu> retour = new ArrayList<Pneu>();

        for (Pneu pneu : pneu_repository.findAll()) {
            log.info(pneu.toString());
            retour.add(pneu);
        }
        //return null;
        return retour;
    }


    @GetMapping("/personalise/moteurs")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public Collection<Moteur> getMoteursPersonalise(){

        Collection<Moteur> retour = new ArrayList<Moteur>();

        for (Moteur moteur : moteur_repository.findAll()) {
            log.info(moteur.toString());
            retour.add(moteur);
        }
        //return null;
        return retour;
    }



    @GetMapping("/personalise/modeles")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public Collection<Modele> getModelesPersonalise(){

        Collection<Modele> retour = new ArrayList<Modele>();

        for (Modele modele : modele_repository.findAll()) {
            log.info(modele.toString());
            retour.add(modele);
        }
        //return null;
        return retour;
    }


    @GetMapping("/personalise/jantes")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public Collection<Jante> getJantesPersonalise(){

        Collection<Jante> retour = new ArrayList<Jante>();

        for (Jante jante : jante_repository.findAll()) {
            log.info(jante.toString());
            retour.add(jante);
        }
        //return null;
        return retour;
    }

    @GetMapping("/personalise/couleurs")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public Collection<Couleur> getCouleursPersonalise(){

        Collection<Couleur> retour = new ArrayList<Couleur>();

        for (Couleur couleur : couleur_repository.findAll()) {
            log.info(couleur.toString());
            retour.add(couleur);
        }
        //return null;
        return retour;
    }


}
