package Personalise.Personalise;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class PersonaliseApplication {

	public static void main(String[] args) {
		SpringApplication.run(PersonaliseApplication.class, args);
	}

	private static final Logger log = LoggerFactory.getLogger(PersonaliseApplication.class);

	@Bean
	public CommandLineRunner demo(Pneu_repository pneu_repository, Moteur_repository moteur_repository,
								  Modele_repository modele_repository,
								  Jante_repository jante_repository,
								  Couleur_repository couleur_repository) {
		return (args) -> {

			pneu_repository.save(new Pneu("pneu_1.jpeg"));
			pneu_repository.save(new Pneu("pneu_2.jpeg"));
			pneu_repository.save(new Pneu("pneu_3.jpeg"));
			pneu_repository.save(new Pneu("pneu_4.jpeg"));

			log.info("-------------------------------");
			log.info("Pneus trouvé avec findAll():");
			for (Pneu pneu : pneu_repository.findAll()) {
				log.info(pneu.getPicture());
			}
			log.info("");

			moteur_repository.save(new Moteur("moteur_1.jpeg"));
			moteur_repository.save(new Moteur("moteur_1.jpeg"));
			moteur_repository.save(new Moteur("moteur_1.jpeg"));
			moteur_repository.save(new Moteur("moteur_1.jpeg"));

			log.info("-------------------------------");
			log.info("Moteurs trouvé avec findAll():");
			for (Moteur moteur : moteur_repository.findAll()) {
				log.info(moteur.getPicture());
			}
			log.info("");


			modele_repository.save(new Modele("lamborghini.jpeg"));
			modele_repository.save(new Modele("cliot.jpeg"));
			modele_repository.save(new Modele("coccinelle.jpeg"));
			modele_repository.save(new Modele("collection.jpeg"));
			modele_repository.save(new Modele("ferrari.jpeg"));
			modele_repository.save(new Modele("porche_1.jpeg"));
			modele_repository.save(new Modele("porche_2.jpeg"));
			modele_repository.save(new Modele("Y10.jpeg"));


			log.info("-------------------------------");
			log.info("Moteurs trouvé avec findAll():");
			for (Modele modele : modele_repository.findAll()) {
				log.info(modele.getPicture());
			}
			log.info("");


			jante_repository.save(new Jante("jante_1.jpeg"));
			jante_repository.save(new Jante("jante_2.jpeg"));
			jante_repository.save(new Jante("jante_3.jpeg"));
			jante_repository.save(new Jante("jante_4.jpeg"));


			log.info("-------------------------------");
			log.info("Moteurs trouvé avec findAll():");
			for (Jante jante : jante_repository.findAll()) {
				log.info(jante.getPicture());
			}
			log.info("");


			couleur_repository.save(new Couleur(230,100,255));
			couleur_repository.save(new Couleur(0,0,255));
			couleur_repository.save(new Couleur(255,0,255));
			couleur_repository.save(new Couleur(255,0,0));
			couleur_repository.save(new Couleur(0,255,255));
			couleur_repository.save(new Couleur(0,0,255));
			couleur_repository.save(new Couleur(0,255,0));


			log.info("-------------------------------");
			log.info("Moteurs trouvé avec findAll():");
			for (Couleur couleur : couleur_repository.findAll()) {
				log.info(couleur.toString());
			}
			log.info("");


		};
	}
}
