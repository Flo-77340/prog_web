package Personalise.Personalise;

import org.springframework.data.repository.CrudRepository;

public interface Pneu_repository extends CrudRepository<Pneu, Long> {
}
