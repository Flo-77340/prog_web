package Personalise.Personalise;


import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
public class Couleur {

    /*
     * IDentifiant
     */
    private Long id;

    @Id
    @Column(name="couleur_id")
    @GeneratedValue(strategy = GenerationType.AUTO) public Long getId()
    {   return id;  }

    public void setId(Long id ){ this.id = id; }



    private int bleu,vert,rouge;

    @Column(name="vert")
    public int getVert(){return vert;}
    public void setVert(int vert){this.vert = vert;}

    @Column(name="bleu")
    public int getBleu(){return bleu;}
    public void setBleu(int bleu){this.bleu = bleu;}

    @Column(name="rouge")
    public int getRouge(){return rouge;}
    public void setRouge(int rouge){this.rouge = rouge;}

    public Couleur(int bleu,int vert,int rouge){
        super();
        //bleu
        if (bleu >=0 & bleu <=255 )this.bleu = bleu;
        else if (bleu <0) this.bleu = 0;
        else this.bleu = 255;
        //vert
        if (vert >=0 & vert <=255 )this.vert = vert;
        else if (vert <0) this.vert = 0;
        else this.vert = 255;
        //rouge
        if (rouge >=0 & rouge <=255 )this.rouge = rouge;
        else if (rouge <0) this.rouge = 0;
        else this.rouge = 255;
    }

    public Couleur(){
        this(0,0,0);
    }


}
