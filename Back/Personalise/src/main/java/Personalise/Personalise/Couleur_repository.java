package Personalise.Personalise;

import org.springframework.data.repository.CrudRepository;

public interface Couleur_repository extends CrudRepository<Couleur, Long> {
}
