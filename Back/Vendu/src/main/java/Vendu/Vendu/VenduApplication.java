package Vendu.Vendu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class VenduApplication {

	private static final Logger log = LoggerFactory.getLogger(VenduApplication.class);


	public static void main(String[] args) {
		SpringApplication.run(VenduApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(Voiture_Vendu_Repository voiture_vendu_repository) {
		return (args) -> {

			Voiture ferary = new Voiture();

			ferary.setCar_name("coccinel");
			ferary.getCarrosserie().getModele().setPicture("coccinelle.jpeg");

			for( Roue r : ferary.getRoues()){
				r.getJante().setPicture("jante_1.jpeg");
				r.getPneu().setPicture("pneu_1.jpeg");
			}

			ferary.getMoteur().setPicture("moteur_1.jpeg");
			ferary.getCarrosserie().getCouleur().setRouge(255);
			voiture_vendu_repository.save(ferary);

			Voiture porche = new Voiture();

			porche.setCar_name("porche");
			porche.getCarrosserie().getModele().setPicture("porche_2.jpeg");

			for( Roue r : porche.getRoues()){
				r.getJante().setPicture("jante_2.jpeg");
				r.getPneu().setPicture("pneu_2.jpeg");
			}

			porche.getMoteur().setPicture("moteur_2.jpeg");
			voiture_vendu_repository.save(porche);

			Voiture cliot = new Voiture();

			cliot.setCar_name("cliot");
			cliot.getCarrosserie().getModele().setPicture("cliot.jpeg");

			for( Roue r : cliot.getRoues()){
				r.getJante().setPicture("jante_3.jpeg");
				r.getPneu().setPicture("pneu_3.jpeg");
			}

			cliot.getMoteur().setPicture("moteur_3.jpeg");
			cliot.getCarrosserie().getCouleur().setBleu(255);
			cliot.getCarrosserie().getCouleur().setRouge(200);
			cliot.getCarrosserie().getCouleur().setVert(200);
			voiture_vendu_repository.save(cliot);


			Voiture lamborgini = new Voiture();

			lamborgini.setCar_name("lamborgini");
			lamborgini.getCarrosserie().getModele().setPicture("lamborghini.jpeg");

			for( Roue r : lamborgini.getRoues()){
				r.getJante().setPicture("jante_4.jpeg");
				r.getPneu().setPicture("pneu_4.jpeg");
			}

			lamborgini.getMoteur().setPicture("moteur_4.jpeg");
			lamborgini.getCarrosserie().getCouleur().setBleu(255);
			lamborgini.getCarrosserie().getCouleur().setRouge(255);
			lamborgini.getCarrosserie().getCouleur().setVert(255);
			voiture_vendu_repository.save(lamborgini);

			log.info("-------------------------------");
			log.info("Voitures trouvé avec findAll():");
			for (Voiture voiture : voiture_vendu_repository.findAll()) {
				log.info(voiture.getCar_name());
			}
			log.info("");






		};
	}

}
