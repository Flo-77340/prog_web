package Vendu.Vendu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;

@RestController
public class Service_Voiture_Vendu {

    private static final Logger log = LoggerFactory.getLogger(VenduApplication.class);

    Voiture_Vendu_Repository voiture_Vendu_Repository;

    @Autowired
    public Service_Voiture_Vendu(Voiture_Vendu_Repository voiture_Vendu_Repository) {
        super();
        this.voiture_Vendu_Repository = voiture_Vendu_Repository;
    }

    @GetMapping("/vendus")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public Collection<Voiture> getVoituresDispo(){

        Collection<Voiture> retour = new ArrayList<Voiture>();

        for (Voiture voiture : voiture_Vendu_Repository.findAll()) {
            log.info(voiture.toString());
            retour.add(voiture);
        }
        //return null;
        return retour;
    }



    @GetMapping(value = "/vendus/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Voiture getVoitureByID(@PathVariable("id") Long id ){
        return voiture_Vendu_Repository.findById(id).get();
    }


    @PutMapping(value = "/vendus")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public void rent(@RequestBody Voiture new_car){
        log.info("post request OK ????");
        voiture_Vendu_Repository.save(new Voiture(new_car));

        log.info("post request OK ....");
    }

}
