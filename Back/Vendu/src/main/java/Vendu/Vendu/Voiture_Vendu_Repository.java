package Vendu.Vendu;

import org.springframework.data.repository.CrudRepository;

public interface Voiture_Vendu_Repository extends CrudRepository<Voiture, Long> {

}
