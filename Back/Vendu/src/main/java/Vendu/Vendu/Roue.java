package Vendu.Vendu;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
public class Roue {


    /*
     * IDentifiant
     */
    private Long id;

    @Id
    @Column(name="roue_id")
    @GeneratedValue(strategy = GenerationType.AUTO) public Long getId()
    {   return id;  }

    public void setId(Long id ){ this.id = id; }


    public Roue( Pneu pneu, Jante jante ){
        super();
        this.setPneu(pneu);
        this.setJante(jante);
    }

    public Roue (Roue roue){
        super();
        this.setPneu(new Pneu(roue.pneu));
        this.setJante(new Jante(roue.jante));
    }

    public Roue( Pneu pneu){
        this(pneu,new Jante());
    }

    public Roue( Jante jante){
        this(new Pneu(),jante);
    }

    public Roue(String picture){
        this(new Pneu(),new Jante());
    }

    public Roue(){
        this(new Pneu(),new Jante());
    }

    /*
     * Voiture associé
     */
    Voiture voiture;
    @ManyToOne
    @JoinColumn(name="voiture_id", referencedColumnName="voiture_id")
    @JsonBackReference
    public Voiture getVoiture (){ return voiture;
    }
    public void setVoiture( Voiture voiture){ this.voiture = voiture;
    }


    /*
     * Pneu associé
     */

    private Pneu pneu;
    @OneToOne(cascade=CascadeType.ALL,mappedBy = "roue")
    public Pneu getPneu(){ return pneu;
    }
    public void setPneu( Pneu pneu ){ this.pneu = pneu;
        pneu.setRoue(this);
    }

    /*
     * Jante associé
     */

    private Jante jante;
    @OneToOne(cascade=CascadeType.ALL,mappedBy = "roue")
    public Jante getJante(){ return jante;
    }
    public void setJante( Jante jante ){ this.jante = jante;
        jante.setRoue(this);
    }
}
