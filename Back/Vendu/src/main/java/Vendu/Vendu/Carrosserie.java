package Vendu.Vendu;


import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
public class Carrosserie {

    /*
     * IDentifiant
     */
    private Long id;

    @Id
    @Column(name="carrosserie_id")
    @GeneratedValue(strategy = GenerationType.AUTO) public Long getId()
    {   return id;  }

    public void setId(Long id ){ this.id = id; }



    public Carrosserie( Couleur couleur, Modele modele ){
        super();
        this.setCouleur( couleur);
        this.setModele(modele);
    }

    public Carrosserie(Couleur couleur){ this(couleur,new Modele()); }

    public Carrosserie( Modele modele){ this(new Couleur(),modele); }


    public Carrosserie(){
        this(new Couleur(),new Modele());
    }

    public Carrosserie(Carrosserie c){
        this(new Couleur(c.getCouleur()),new Modele(c.getModele()));
    }



    /*
     * Voiture associé
     */
    Voiture voiture;
    @OneToOne
    @JoinColumn(name="voiture_id", referencedColumnName="voiture_id")
    @JsonBackReference
    public Voiture getVoiture (){ return voiture;
    }
    public void setVoiture( Voiture voiture){ this.voiture = voiture;
    }


    /*
     * Modele associé
     */

    private Modele modele;
    @OneToOne(cascade=CascadeType.ALL,mappedBy = "carrosserie")
    public Modele getModele(){ return modele;
    }
    public void setModele(Modele modele ){
        this.modele = modele;
        modele.setCarrosserie(this);
    }


    /*
     * Couleur associé
     */

    private Couleur couleur;
    @OneToOne(cascade=CascadeType.ALL,mappedBy = "carrosserie")
    public Couleur getCouleur(){ return couleur;
    }
    public void setCouleur( Couleur couleur ){ this.couleur = couleur;
        couleur.setCarrosserie(this);
    }
}


