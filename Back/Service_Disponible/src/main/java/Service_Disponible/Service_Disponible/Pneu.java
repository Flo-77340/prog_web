package Service_Disponible.Service_Disponible;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
public class Pneu {


    /*
     * IDentifiant
     */
    private Long id;

    @Id
    @Column(name="pneu_id")
    @GeneratedValue(strategy = GenerationType.AUTO) public Long getId()
    {   return id;  }

    public void setId(Long id ){ this.id = id; }

    private String picture;
    @Column(name="picture_Name")
    public String getPicture(){return picture;}
    public void setPicture(String picture){this.picture = picture;}

    public Pneu(String picture){
        super();
        this.picture = picture;
    }

    public Pneu(Pneu pneu){
        super();
        this.picture = pneu.picture;
    }

    public Pneu(){
        this("Pneu.default.format");
    }


    /*
     * Roue associé
     */

    private Roue roue;
    @OneToOne()
    @JoinColumn(name="roue_id", referencedColumnName="roue_id")
    @JsonBackReference
    public Roue getRoue(){ return roue;
    }
    public void setRoue( Roue roue ){ this.roue = roue;
    }

}

