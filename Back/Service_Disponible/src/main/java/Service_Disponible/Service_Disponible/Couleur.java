package Service_Disponible.Service_Disponible;


import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
public class Couleur {

    /*
     * IDentifiant
     */
    private Long id;

    @Id
    @Column(name="couleur_id")
    @GeneratedValue(strategy = GenerationType.AUTO) public Long getId()
    {   return id;  }

    public void setId(Long id ){ this.id = id; }



    private int bleu,vert,rouge;

    @Column(name="vert")
    public int getVert(){return vert;}
    public void setVert(int vert){
        if (vert >=0 & vert <=255 )this.vert = vert;
        else if (vert <0) this.vert = 0;
        else this.vert = 255;
    }

    @Column(name="bleu")
    public int getBleu(){return bleu;}
    public void setBleu(int bleu){
        if (bleu >=0 & bleu <=255 )this.bleu = bleu;
        else if (bleu <0) this.bleu = 0;
        else this.bleu = 255;
    }

    @Column(name="rouge")
    public int getRouge(){return rouge;}
    public void setRouge(int rouge){
        if (rouge >=0 & rouge <=255 )this.rouge = rouge;
        else if (rouge <0) this.rouge = 0;
        else this.rouge = 255;
    }

    public Couleur(int bleu,int vert,int rouge){
        super();
        //bleu
        if (bleu >=0 & bleu <=255 )this.bleu = bleu;
        else if (bleu <0) this.bleu = 0;
        else this.bleu = 255;
        //vert
        if (vert >=0 & vert <=255 )this.vert = vert;
        else if (vert <0) this.vert = 0;
        else this.vert = 255;
        //rouge
        if (rouge >=0 & rouge <=255 )this.rouge = rouge;
        else if (rouge <0) this.rouge = 0;
        else this.rouge = 255;
    }

    public Couleur(){
        this(0,0,0);
    }


    /*
     * Carosserie associé
     */

    private Carrosserie carrosserie;
    @OneToOne()
    @JoinColumn(name="carrosserie_id", referencedColumnName="carrosserie_id")
    @JsonBackReference
    public Carrosserie getCarrosserie(){ return carrosserie;
    }
    public void setCarrosserie( Carrosserie carrosserie ){ this.carrosserie = carrosserie;
    }
}
