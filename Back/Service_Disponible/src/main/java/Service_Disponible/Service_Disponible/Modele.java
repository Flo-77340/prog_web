package Service_Disponible.Service_Disponible;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
public class Modele {

    /*
     * IDentifiant
     */
    private Long id;

    @Id
    @Column(name="modele_id")
    @GeneratedValue(strategy = GenerationType.AUTO) public Long getId()
    {   return id;  }

    public void setId(Long id ){ this.id = id; }

    private String picture;
    @Column(name="picture_Name")
    public String getPicture(){return picture;}
    public void setPicture(String picture){this.picture = picture;}

    public Modele(String picture){
        super();
        this.picture = picture;
    }

    public Modele(){
        this("Jante.default.format");
    }





    /*
     * Carosserie associé
     */

    private Carrosserie carrosserie;
    @OneToOne()
    @JoinColumn(name="carrosserie_id", referencedColumnName="carrosserie_id")
    @JsonBackReference
    public Carrosserie getCarrosserie(){ return carrosserie;
    }
    public void setCarrosserie( Carrosserie carrosserie ){ this.carrosserie = carrosserie;
    }
}
