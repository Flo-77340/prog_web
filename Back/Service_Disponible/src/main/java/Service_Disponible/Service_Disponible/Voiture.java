package Service_Disponible.Service_Disponible;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
public class Voiture {


    /*
     * IDentifiant
     */
    private Long id;

    @Id
    @Column(name="voiture_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId()
    {   return id;  }

    public void setId(Long id ){ this.id = id; }

    public Voiture(){
        this(new Moteur(), new Roue(), new Carrosserie());
    }


    public Voiture(Moteur moteur, Roue roue, Carrosserie carrosserie ){
        super();
        this.setMoteur(moteur);

        this.roues = new ArrayList<Roue>();

        Collection<Roue> roues_ = new ArrayList<Roue>();
        roues_.add(roue);
        roues_.add(new Roue(roue));
        roues_.add(new Roue(roue));
        roues_.add(new Roue(roue));

        this.addRoues(roues_);


        this.setCarrosserie(carrosserie);
    }



    private String car_name;
    @Column(name="picture_Name")
    public String getCar_name(){return car_name;}
    public void setCar_name(String car_name){this.car_name = car_name;}



    /*
     * Roues associé
     */

    private Collection<Roue> roues;

    @OneToMany(cascade=CascadeType.ALL, mappedBy="voiture")
    @Column(name="Roues_id")
    public Collection<Roue> getRoues(){ return roues;
    }
    public void setRoues(Collection<Roue> roues){ this.roues = roues;
    }
    public void addRoue( Roue roue ){ this.getRoues().add( roue );
        roue.setVoiture(this); }
    public void addRoues(Collection<Roue> roues){
        for(Roue r : roues){
            this.addRoue(r);
        }
    }

    /*
     * Moteur associé
     */

    private Moteur moteur;
    @OneToOne(cascade=CascadeType.ALL,mappedBy = "voiture")
    public Moteur getMoteur(){ return moteur;
    }
    public void setMoteur( Moteur moteur ){ this.moteur = moteur;
    moteur.setVoiture(this);
    }

    /*
     * Carosserie associé
     */

    private Carrosserie carrosserie;
    @OneToOne(cascade=CascadeType.ALL,mappedBy = "voiture")
    public Carrosserie getCarrosserie(){ return carrosserie;
    }
    public void setCarrosserie( Carrosserie carrosserie ){ this.carrosserie = carrosserie;
        carrosserie.setVoiture(this);
    }

}
