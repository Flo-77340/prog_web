package Service_Disponible.Service_Disponible;

import org.springframework.data.repository.CrudRepository;

public interface Voiture_Dispo_Repository extends CrudRepository<Voiture, Long> {

}
