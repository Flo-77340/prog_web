package Service_Disponible.Service_Disponible;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;

@RestController
public class Service_Voiture_Dispo {

    private static final Logger log = LoggerFactory.getLogger(ServiceDisponibleApplication.class);

    Voiture_Dispo_Repository voiture_Dispo_Repository;

    @Autowired
    public Service_Voiture_Dispo(Voiture_Dispo_Repository voiture_Dispo_Repository) {
        super();
        this.voiture_Dispo_Repository = voiture_Dispo_Repository;
    }

    @GetMapping("/disponibles")
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public Collection<Voiture> getVoituresDispo(){

        Collection<Voiture> retour = new ArrayList<Voiture>();

        for (Voiture voiture : voiture_Dispo_Repository.findAll()) {
            log.info(voiture.toString());
            retour.add(voiture);
        }
        //return null;
        return retour;
    }

    @GetMapping(value = "/disponibles/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public Voiture getVoitureByID(@PathVariable("id") Long id ){
        return voiture_Dispo_Repository.findById(id).get();
    }
}
