#!/bin/bash

#Remove previous deployments, services and ingress
sudo kubectl delete deployments --all
sudo kubectl delete services --all
sudo kubectl delete ingress --all

#Remove claims before volumes
sudo kubectl delete pvc --all
sudo kubectl delete pv --all

#Loads volumes before claim to atribute each claim to it respectly volumes

sudo kubectl apply -f disponible-volume.yml
sudo kubectl apply -f disponible-claim.yml

sudo kubectl apply -f personalise-volume.yml
sudo kubectl apply -f personalise-claim.yml

sudo kubectl apply -f vendu-volume.yml
sudo kubectl apply -f vendu-claim.yml

#Loads deployments 
sudo kubectl apply -f registry.yml
sudo kubectl apply -f vendu.yml
sudo kubectl apply -f personalise.yml
sudo kubectl apply -f disponible.yml
sudo kubectl apply -f front.yml

#Loads Services
sudo kubectl apply -f registry-services.yml
sudo kubectl apply -f vendu-services.yml
sudo kubectl apply -f personalise-services.yml
sudo kubectl apply -f disponible-services.yml
sudo kubectl apply -f front-services.yml


#Load ingress front to back 
sudo kubectl apply -f front-to-back-ingress.yml