package Registry.Registry;

import com.sun.xml.bind.v2.schemagen.xmlschema.Any;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
public class FrontwebserviceApplication {
    @Autowired
    DiscoveryClient discoveryClient;

    private static final Logger log = LoggerFactory.getLogger(FrontwebserviceApplication.class);


    /*
        ______________________________________________________________________________________
        ______________________________________________________________________________________
                                       Voitures Disponibles
        ______________________________________________________________________________________
        ______________________________________________________________________________________
     */

    @CrossOrigin(origins = "*")
    @GetMapping("/disponibles")
    public String hello() {

        log.info("test");
        List<ServiceInstance> instances = discoveryClient.getInstances("service disponible");
        ServiceInstance test = instances.get(0);
        String hostname = test.getHost();
        int port = test.getPort();
        RestTemplate restTemplate = new RestTemplate();
        String microservice1Address = "http://" + hostname + ":" + port + "/disponibles";
        ResponseEntity<String> response =
                restTemplate.getForEntity(microservice1Address, String.class);
        String s = response.getBody();

        log.info("LE get est passé ");
        return s;

    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/disponibles/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String getVoitureByID(@PathVariable("id") Long id) {

        log.info("test");
        List<ServiceInstance> instances = discoveryClient.getInstances("service disponible");
        ServiceInstance test = instances.get(0);
        String hostname = test.getHost();
        int port = test.getPort();
        RestTemplate restTemplate = new RestTemplate();
        String microservice1Address = "http://" + hostname + ":" + port + "/disponibles/" + id;

        log.info("test 2 :)");
        log.info(microservice1Address);
        ResponseEntity<String> response =
                restTemplate.getForEntity(microservice1Address, String.class);
        String s = response.getBody();

        log.info("LE get est passé ");
        return s;

    }

    /*
        ______________________________________________________________________________________
        ______________________________________________________________________________________
                                                Vendus
        ______________________________________________________________________________________
        ______________________________________________________________________________________
     */



    /*
        ______________________________________________________________________________________
        ______________________________________________________________________________________
                                                Personalisé
        ______________________________________________________________________________________
        ______________________________________________________________________________________
     */

    @CrossOrigin(origins = "*")
    @GetMapping("/personalise/pneus")
    public String pneus_personalise() {

        log.info("test");
        List<ServiceInstance> instances = discoveryClient.getInstances("service Personalise");
        ServiceInstance test = instances.get(0);
        String hostname = test.getHost();
        int port = test.getPort();
        RestTemplate restTemplate = new RestTemplate();
        String microservice1Address = "http://" + hostname + ":" + port + "/personalise/pneus";
        ResponseEntity<String> response =
                restTemplate.getForEntity(microservice1Address, String.class);
        String s = response.getBody();

        log.info("LE get est passé ");
        return s;

    }

    @CrossOrigin(origins = "*")
    @GetMapping("/personalise/moteurs")
    public String moteurs_personalise() {

        log.info("test");
        List<ServiceInstance> instances = discoveryClient.getInstances("service Personalise");
        ServiceInstance test = instances.get(0);
        String hostname = test.getHost();
        int port = test.getPort();
        RestTemplate restTemplate = new RestTemplate();
        String microservice1Address = "http://" + hostname + ":" + port + "/personalise/moteurs";
        ResponseEntity<String> response =
                restTemplate.getForEntity(microservice1Address, String.class);
        String s = response.getBody();

        log.info("LE get est passé ");
        return s;

    }

    @CrossOrigin(origins = "*")
    @GetMapping("/personalise/modeles")
    public String modeles_personalise() {

        log.info("test");
        List<ServiceInstance> instances = discoveryClient.getInstances("service Personalise");
        ServiceInstance test = instances.get(0);
        String hostname = test.getHost();
        int port = test.getPort();
        RestTemplate restTemplate = new RestTemplate();
        String microservice1Address = "http://" + hostname + ":" + port + "/personalise/modeles";
        ResponseEntity<String> response =
                restTemplate.getForEntity(microservice1Address, String.class);
        String s = response.getBody();

        log.info("LE get est passé ");
        return s;

    }

    @CrossOrigin(origins = "*")
    @GetMapping("/personalise/jantes")
    public String jantes_personalise() {

        log.info("test");
        List<ServiceInstance> instances = discoveryClient.getInstances("service Personalise");
        ServiceInstance test = instances.get(0);
        String hostname = test.getHost();
        int port = test.getPort();
        RestTemplate restTemplate = new RestTemplate();
        String microservice1Address = "http://" + hostname + ":" + port + "/personalise/jantes";
        ResponseEntity<String> response =
                restTemplate.getForEntity(microservice1Address, String.class);
        String s = response.getBody();

        log.info("LE get est passé ");
        return s;

    }

    @CrossOrigin(origins = "*")
    @GetMapping("/personalise/couleurs")
    public String couleurs_personalise() {

        log.info("test");
        List<ServiceInstance> instances = discoveryClient.getInstances("service Personalise");
        ServiceInstance test = instances.get(0);
        String hostname = test.getHost();
        int port = test.getPort();
        RestTemplate restTemplate = new RestTemplate();
        String microservice1Address = "http://" + hostname + ":" + port + "/personalise/couleurs";
        ResponseEntity<String> response =
                restTemplate.getForEntity(microservice1Address, String.class);
        String s = response.getBody();

        log.info("LE get est passé ");
        return s;


    }

}
