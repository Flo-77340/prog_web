import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-classes',
  templateUrl: './classes.component.html',
  styleUrls: ['./classes.component.css']
})
export class ClassesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

export class Couleur{
  id : number = 1;
  bleu : number;
  vert : number;
  rouge : number;

  constructor() { }
}

export class Modele{
  id : number = 1;
  picture : string; 

  constructor() { }
}

export class Carrosserie {
  id : number = 1;
  modele : Modele;
  couleur : Couleur;

  constructor() { }
}

export class Jante{
  id : number = 1;
  picture : string; 

  constructor() { }
}

export class Pneu{
  id : number = 1;
  picture : string; 

  constructor() { }
}

export class Roue{
  id : number = 1;
  pneu : Pneu;
  jante : Jante;

  constructor() { }
}

export class Moteur{
  id : number = 1;
  picture : string; 

  constructor() { }
}


export class Voiture{
  id : number = 1;
  car_name : string;
  roues : Roue[]; 
  moteur : Moteur; 
  carrosserie : Carrosserie;

  constructor() { }
}


/*
  ______________________________________________________________________________________
  ______________________________________________________________________________________
                                      Personalisé
  ______________________________________________________________________________________
  ______________________________________________________________________________________
*/



export class Couleur_Perso{
  id : number = 1;
  bleu : number;
  vert : number;
  rouge : number;

  constructor() { }
}

export class Modele_Perso{
  id : number = 1;
  picture : string; 

  constructor() { }
}

export class Jante_Perso{
  id : number = 1;
  picture : string; 

  constructor() { }
}

export class Pneu_Perso{
  id : number = 1;
  picture : string; 

  constructor() { }
}


export class Moteur_Perso{
  id : number = 1;
  picture : string; 

  constructor() { }
}
