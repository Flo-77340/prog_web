import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VenduComponent } from './vendu.component';

describe('VenduComponent', () => {
  let component: VenduComponent;
  let fixture: ComponentFixture<VenduComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VenduComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VenduComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
