import { Component, OnInit } from '@angular/core';
import { Voiture } from '../classes/classes.component';
import { ConfigService } from '../get-back/get-back.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vendu',
  templateUrl: './vendu.component.html',
  styleUrls: ['./vendu.component.css']
})
export class VenduComponent implements OnInit {

  voitures_dispo : Voiture[] = []; 

  //constructor(private configService:ConfigService) { }
  constructor(private service : ConfigService,
    private router: Router) { 
  }

  ngOnInit(): void {
    this.service.getCarsVendus().subscribe((data:Voiture[])=>this.voitures_dispo = data);
  }

  get_Car_by_id(id:number) : Voiture {
    this.voitures_dispo.forEach(car => {
      if (car.id == id ){
        return car;
      }
    });

    return null; 
  }

}
