import { Component, OnInit, Injectable } from '@angular/core';
import { Voiture } from '../classes/classes.component';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { DisponibleComponent } from '../disponible/disponible.component';
import { ConfigService } from '../get-back/get-back.component';

@Injectable()
@Component({
  selector: 'app-details-voitures',
  templateUrl: './details-voitures.component.html',
  styleUrls: ['./details-voitures.component.css']
})
export class DetailsVoituresComponent implements OnInit {

  constructor( private route: ActivatedRoute,
    private service : ConfigService,
    private router : Router,) { }

  car_id:number; 
  car : Voiture;

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.car_id = +params['id']; // (+) converts string 'id' to a number

      // In a real app: dispatch action to load the details here.
   });
    this.service.getCarsDisponiblesById(this.car_id).subscribe((data:Voiture)=>this.car = data);
    
  }
  achette_voiture() : void {

    
    alert("Bravos pour votre achat");
    this.service.putVoiture();
    
    
    this.router.navigate(['vendus']);
  }


}
