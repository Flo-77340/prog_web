import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsVoituresComponent } from './details-voitures.component';

describe('DetailsVoituresComponent', () => {
  let component: DetailsVoituresComponent;
  let fixture: ComponentFixture<DetailsVoituresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsVoituresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsVoituresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
