import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../get-back/get-back.component';
import { Couleur_Perso, Jante_Perso, Modele_Perso, Moteur_Perso, Pneu_Perso, Voiture, Roue } from '../classes/classes.component';
import { Router } from '@angular/router';



@Component({
  selector: 'app-personalise',
  templateUrl: './personalise.component.html',
  styleUrls: ['./personalise.component.css'],

})
export class PersonaliseComponent implements OnInit{

  constructor(private service : ConfigService,
    private router : Router) { }


  couleurs : Couleur_Perso [];
  jantes : Jante_Perso[];
  modeles : Modele_Perso[];
  moteurs :Moteur_Perso[];
  pneus : Pneu_Perso[];

  ngOnInit(): void {
    
    this.service.getCouleursPerso().subscribe((data:Couleur_Perso[])=>this.couleurs = data);
    this.service.getJantesPerso().subscribe((data:Jante_Perso[])=>this.jantes = data);
    this.service.getModelsPerso().subscribe((data:Modele_Perso[])=>this.modeles = data);
    this.service.getMoteursPerso().subscribe((data:Modele_Perso[])=>this.moteurs = data);
    this.service.getPneusPerso().subscribe((data:Pneu_Perso[])=>this.pneus = data);
    
  }

  achette_voiture() : void {

    
    alert("Bravos pour votre achat");
    this.service.putVoiture();
    
    
    this.router.navigate(['vendus']);
  }


}

