import { Component, OnInit, ViewChild, ContentChildren, ElementRef, ViewChildren, QueryList, Input, AfterViewInit, Directive, TemplateRef } from '@angular/core';
import { AnimationBuilder, AnimationPlayer, AnimationFactory, style, animate } from '@angular/animations';



@Directive({
  selector: '[carouselItem]'
})
export class CarouselItemDirective {

  constructor( public tpl : TemplateRef<any> ) {
  }

}


@Directive({
  selector: '.carousel-item'
})
export class CarouselItemElement {
}



@Component({
  selector: 'carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css'],
  styles: [`
    .carousel-wrapper {
      overflow: hidden;
    }
    .carousel {
      list-style: none;
      margin: 0;
      padding: 0;
      width: 6000px;
    }
    .carousel-inner {
      display: flex;
    }
 `],
})
export class CarouselComponent implements OnInit, AfterViewInit {


  ngOnInit(): void {
  }

  @ViewChild('carousel') private carousel : ElementRef;
  @ContentChildren(CarouselItemDirective) items : QueryList<CarouselItemDirective>;
  @ViewChildren(CarouselItemElement, { read: ElementRef }) private itemsElements : QueryList<ElementRef>;

  constructor(private builder : AnimationBuilder,) { }


  ngAfterViewInit() {
    this.itemWidth = this.itemsElements.first.nativeElement.getBoundingClientRect().width;
    this.carouselWrapperStyle = {
      width: `${this.itemWidth}px`
    }
  }

  carouselWrapperStyle = {}

  @Input() timing = '250ms ease-in';
  @Input() showControls = true;
  private player : AnimationPlayer;
  private itemWidth : number;
  private currentSlide = 0;

  next() {
    if( this.currentSlide + 1 === this.items.length ) return;

    this.currentSlide = (this.currentSlide + 1) % this.items.length;

    const offset = this.currentSlide * this.itemWidth;

    const myAnimation : AnimationFactory = this.builder.build([
       animate(this.timing, style({ transform: `translateX(-${offset}px)` }))
    ]);

    this.player = myAnimation.create(this.carousel.nativeElement);
    this.player.play();
  }

  prev() {
    if( this.currentSlide === 0 ) return;
 
     this.currentSlide = ((this.currentSlide - 1) + this.items.length) % this.items.length;
     const offset = this.currentSlide * this.itemWidth;
 
     const myAnimation : AnimationFactory = this.builder.build([
       animate(this.timing, style({ transform: `translateX(-${offset}px)` }))
     ]);
 
     this.player = myAnimation.create(this.carousel.nativeElement);
     this.player.play();
   }

}

