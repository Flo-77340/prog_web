import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Voiture, Pneu, Pneu_Perso, Moteur_Perso, Modele_Perso, Jante_Perso, Couleur_Perso } from '../classes/classes.component';


@Injectable()
export class ConfigService {
  constructor(private httpClient: HttpClient) { }

  private ip="to-back.localhost";
  //private ip ="localhost:8761"

  getCarsDisponibles(){
    return this.httpClient.get<Voiture[]>("http://"+this.ip+"/disponibles");
  }

  getCarsVendus(){
    return this.httpClient.get<Voiture[]>("http://"+this.ip+"/vendus/vendus");
  }

  getCarsVenduById(id : number){
    return this.httpClient.get<Voiture>("http://"+this.ip+"/vendus/vendus/"+id);
  }


  getCarsDisponiblesById(id : number){
    return this.httpClient.get<Voiture>("http://"+this.ip+"/disponibles/"+id);
  }

  getPneusPerso(){
    return this.httpClient.get<Pneu_Perso[]>("http://"+this.ip+"/personalise/pneus");
  }

  getMoteursPerso(){
    return this.httpClient.get<Moteur_Perso[]>("http://"+this.ip+"/personalise/moteurs");
  }

  getModelsPerso(){
    return this.httpClient.get<Modele_Perso[]>("http://"+this.ip+"/personalise/modeles");
  }

  getJantesPerso(){
    return this.httpClient.get<Jante_Perso[]>("http://"+this.ip+"/personalise/jantes");
  }

  getCouleursPerso(){
    return this.httpClient.get<Couleur_Perso[]>("http://"+this.ip+"/personalise/couleurs");
  }

  putVoiture(){
    let v:Voiture;
    this.getCarsVenduById(1).subscribe((data:Voiture)=>v = data);
    let body = JSON.stringify({ v });   
    return this.httpClient.put("http://"+this.ip+"/vendus/vendus",body);
    
  }


}

@Component({
  selector: 'app-get-back',
  templateUrl: './get-back.component.html',
  styleUrls: ['./get-back.component.css']
})
export class GetBackComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
