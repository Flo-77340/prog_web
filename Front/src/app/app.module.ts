import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DisponibleComponent } from './disponible/disponible.component';
import { PresentationComponent } from './presentation/presentation.component';

import { Routes } from '@angular/router';
import { RouterModule } from '@angular/router';
import { ClassesComponent, Voiture } from './classes/classes.component';
import { GetBackComponent, ConfigService } from './get-back/get-back.component';
import { DetailsVoituresComponent } from './details-voitures/details-voitures.component';
import { PersonaliseComponent } from './personalise/personalise.component';
import { CarouselComponent } from './carousel/carousel.component';
import { VenduComponent } from './vendu/vendu.component';
import { DetailVenduComponent } from './detail-vendu/detail-vendu.component';


const appRoutes: Routes = [
  { path: 'disponibles', component: DisponibleComponent },
  { path: '', component: PresentationComponent },
  { path: 'details', component: DetailsVoituresComponent, data: Voiture },
  { path: 'personalise', component: PersonaliseComponent},
  { path: 'details-vendus', component: DetailVenduComponent, data: Voiture },
  { path: 'vendus', component: VenduComponent },
  
];

@NgModule({
  declarations: [
    AppComponent,
    DisponibleComponent,
    PresentationComponent,
    ClassesComponent,
    GetBackComponent,
    DetailsVoituresComponent,
    PersonaliseComponent,
    CarouselComponent,
    VenduComponent,
    DetailVenduComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
  ],
  providers: [ConfigService,CarouselComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
