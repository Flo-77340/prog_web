import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../get-back/get-back.component';
import { Voiture } from '../classes/classes.component';

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';


@Injectable()
@Component({
  selector: 'app-disponible',
  templateUrl: './disponible.component.html',
  styleUrls: ['./disponible.component.css']
})
export class DisponibleComponent implements OnInit {

  voitures_dispo : Voiture[] = []; 

  //constructor(private configService:ConfigService) { }
  constructor(private service : ConfigService,
    private router: Router) { 
  }

  ngOnInit(): void {
    this.service.getCarsDisponibles().subscribe((data:Voiture[])=>this.voitures_dispo = data);
  }

  get_Car_by_id(id:number) : Voiture {
    this.voitures_dispo.forEach(car => {
      if (car.id == id ){
        return car;
      }
    });

    return null; 
  }

  goToCarDetails(voiture : Voiture){
    this.router.navigate(['/details', { car: voiture  }]);
  }

}
