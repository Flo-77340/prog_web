import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailVenduComponent } from './detail-vendu.component';

describe('DetailVenduComponent', () => {
  let component: DetailVenduComponent;
  let fixture: ComponentFixture<DetailVenduComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailVenduComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailVenduComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
