import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConfigService } from '../get-back/get-back.component';
import { Voiture } from '../classes/classes.component';

@Component({
  selector: 'app-detail-vendu',
  templateUrl: './detail-vendu.component.html',
  styleUrls: ['./detail-vendu.component.css']
})
export class DetailVenduComponent implements OnInit {

  constructor( private route: ActivatedRoute,
    private service : ConfigService,) { }

  car_id:number; 
  car : Voiture;

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.car_id = +params['id']; // (+) converts string 'id' to a number

      // In a real app: dispatch action to load the details here.
   });
    this.service.getCarsVenduById(this.car_id).subscribe((data:Voiture)=>this.car = data);
    
  }

}
